# jaypeg

jaypeg.website is an ultra-lean ephemeral image host made by hackers for hackers. 

# Goals
- Low cost!! Under $100/mo but still w/ free uploads? Need to do math on what that gets me on digital ocean.
- Fast for US East
- No ads
- Easy to for other programmers to use

# Notes on Digital Ocean costs

- Spaces + CDN is $5/mo for 250GiB, then $0.02 per GB storage (50GB/USD)
- **Spaces to Droplet is free but counts towards quota**
- Spaces inbound bandwidth is included/free
- Spaces include 1 TB/mo free, then $0.01 per GB after (100GB/USD)

For $100 month that gets you:
- Spaces sub + 750GiB storage = 1TiB storage for $20/mo, not great deal tbh
- 8GB RAM / 4vCPUs / 160GB Disk + 5 TB transfer = $40/mo, covers 80/20 rule
- $40/mo for microservices (deduping) + bandwidth surplus fees

## TODO
### Essential
- Need to store image files on CDN cache!
- AI for deduping! harvest images and then generate compressed/resized variants, embed and compare 
- **Safely** parse jpeg/png/bmp/TIFF ... convert them all to JPEG XL? Maybe have to look at [wuffs](https://github.com/google/wuffs)
- Log IPs for legal reasons?


## Nice to have
- Tracking Analytics?
- NSFW filter to ban illicit content? [This is a good starting point](https://github.com/alex000kim/nsfw_data_scraper)


## Appreciation Sections

Many thanks to the following users for contributing to jaypeg's codebase quality:
- Discord User Strœmඞ#0420 (Code Review)