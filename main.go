package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/base64"
	"image"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	// add support for different image formats
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
)

const (
	LocalImageDirectory string = "img/"
)

//UserImageRecord wraps imgage data + metadata across Jaypeg's codebase
type UserImageRecord struct {
	ID              string       //Base64-encoded SHA256 checksum, unique to that image
	ArrivalDateUnix time.Time    //Time when image was recieved by backend
	Name            string       //The given name of the file when it was uploaded
	Format          string       //Image format of the file (JPEG/PNG/GIF)
	Config          image.Config //Image metadata lifted upon ingress
	Contents        []byte       //The actual image contents
}

//loadLandingPage fetches the landing page HTML
func loadLandingPage(filename string) (string, error) {
	fp, err := os.OpenFile(filename, os.O_RDONLY, 0755)
	if err != nil {
		return "", err
	}
	defer fp.Close()
	var buf strings.Builder
	_, err = io.Copy(&buf, fp)
	return buf.String(), err
}

//saveImageToDisk writes the image file associated with the record to disk
func saveImageToDisk(imgData UserImageRecord, destFilename string) error {
	fp, err := os.Create(destFilename)
	if err != nil {
		return err
	}
	defer fp.Close()
	bytesWritten, err := fp.Write(imgData.Contents)
	log.Default().Printf("Wrote %d bytes to disk as %s", bytesWritten, destFilename)
	if err != nil {
		return err
	}
	err = fp.Sync()
	if err != nil {
		return err
	}
	return nil
}

//ServeLandingPage sends the first page users will see
func serveLandingPage(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	landingPage, err := loadLandingPage("landing_page.html")
	if err != nil {
		log.Fatal(err)
	}
	_, err = io.WriteString(w, landingPage)
	if err != nil {
		log.Fatal(err)
	}
}

//LogImage prints a recently arrived image to the log
func logImage(img UserImageRecord) {
	log.Default().Printf("Got img name: %s at %s", img.Name, img.ArrivalDateUnix)
	log.Default().Printf("Image is %d x %d %s\n", img.Config.Width, img.Config.Height, img.Format)
}

//CollectUploadedImages is the main ingress point for all new images.
//
// First, use ContentLength for sizing the fileparsing buffer, otherwise
// default to 128MiB buffer.
func collectUploadedImage(w http.ResponseWriter, r *http.Request) {
	if r.ContentLength > 0 && r.ContentLength < 1<<29 {
		r.ParseMultipartForm(r.ContentLength)
	} else {
		r.ParseMultipartForm(1 << 28)
	}
	imgHandle, err := r.MultipartForm.File["filename"][0].Open()
	if err != nil {
		log.Fatal(err)
	}
	defer imgHandle.Close()
	imgData, err := io.ReadAll(imgHandle)
	sha256Sum := sha256.Sum256(imgData)
	if err != nil {
		log.Fatal(err)
	}
	conf, format, err := image.DecodeConfig(bytes.NewReader(imgData))
	img := UserImageRecord{
		ID:              base64.URLEncoding.EncodeToString(sha256Sum[:]),
		ArrivalDateUnix: time.Now(),
		Name:            r.MultipartForm.File["filename"][0].Filename,
		Format:          format,
		Config:          conf,
		Contents:        imgData,
	}
	logImage(img)
	err = saveImageToDisk(img, LocalImageDirectory+img.ID+"."+img.Format)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	http.Handle("/img/", http.FileServer(http.Dir("")))
	http.HandleFunc("/upload", collectUploadedImage)
	http.HandleFunc("/", serveLandingPage)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		panic(err)
	}
}
